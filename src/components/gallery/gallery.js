$(function(){

  if($('.gallery--js').length >= 1){

    var thumbnailSlider = $('.thumbnail-slider--js');
    var largeImageSlider = $('.large-image-slider--js');
    var thumbs = thumbnailSlider.find('.thumbnail--js');
    var loading = largeImageSlider.html();
    var htmlString = '';

    loadLargeImages();
    loadGalerySliders();

    function loadLargeImages(){
      var data;
      for (var i = 0; i < thumbs.length; i++) {
        data = $(thumbs[i]).data();

        htmlString += '<figure>';
        htmlString += '<img data-lazy="'+data.src+'" >';
        htmlString += '<figcaption>';
        htmlString += data.caption;
        htmlString += '</figcaption>';
        htmlString += '</figure>';
      }
      largeImageSlider.html(htmlString);
    }

    function loadGalerySliders(){
      thumbnailSlider.slick({
        autoplay: false,
        infinite: false,
        arrows: false,
        slidesToShow: 10,
        slidesToScroll: 10
      });

      largeImageSlider.slick({
        autoplay: false,
        infinite: true,
        lazyLoad: 'ondemand',
        slidesToShow: 1
      });

      largeImageSlider.append(loading);

      largeImageSlider.on('afterChange', function(slick, currentSlide){
        $('.active').removeClass('active');
        $(thumbs[largeImageSlider.slick('slickCurrentSlide')]).addClass('active');
      });

      $('.slick-slide').on('click' , function() {
        largeImageSlider.slick('slickGoTo',$(this).data('slick-index'));
      });
    }

  }


});
