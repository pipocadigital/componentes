$(function () {
	var $videoThumbnail = $('.video-thumnail--js');
	var thumbnail;
	var data;
	var i;

	for (i = 0; i < $videoThumbnail.length; i++) {
		data = $videoThumbnail.eq(i).data();
		thumbnail = (data.thumbnail) ? data.thumbnail : getYoutubeThumbnail(data.id, data.thumbnailQuality);
		$videoThumbnail.eq(i).find('.play').css('background-image', 'url(' + thumbnail + ')');
	}

	$videoThumbnail.on('click', function () {
		$(this).addClass('playing');
		$(this).find('iframe').attr('src', '//www.youtube.com/embed/' + $(this).data().id + '?rel=0&amp;showinfo=0&amp;autoplay=1');
	});

	function getYoutubeThumbnail(id, quality) {
		var youtubeQuality = ['default', 'mqdefault', 'hqdefault', 'sddefault', 'maxresdefault'];
		if (!quality && quality !== 0) {
			quality = 1;
		}

		if (quality > 4) {
			console.warn('Max thumbnail video quality is 4');
			quality = 4;
		}

		return 'http://img.youtube.com/vi/' + id + '/' + youtubeQuality[quality] + '.jpg';
	}


	// Section Videos
	var $sectionVideos = $('.section-videos--js');
	var $mainVideo = $sectionVideos.find('.main-video--js');
	var $itemVideo = $sectionVideos.find('.thumbnail-video--js');
	var fields = $mainVideo.data('fields').split('|');

	if ($sectionVideos.length) {
		setMainVideo($itemVideo.eq(0), 0);

		$itemVideo.on('click', function () {
			setMainVideo($(this));
		});
	}

	function setMainVideo(element, autoplay) {

		autoplay = (autoplay === 0) ? 0 : 1;
		var data = element.data();

		if (fields.sort().toString() !== Object.keys(data).sort().toString()) {

			throw Error('Data format invalid, verify the required fields');

		} else {

			$itemVideo.removeClass('current');
			element.addClass('current');
			var i;
			for (i = 0; i < fields.length; i++) {
				$mainVideo.find('.' + fields[i] + '-video--js').html(data[fields[i]]);
			}

			$mainVideo.find('iframe').attr('src', '//www.youtube.com/embed/' + data.id + '?rel=0&amp;showinfo=0&amp;autoplay=' + autoplay);

		}
	}


});
