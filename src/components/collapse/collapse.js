"use strict";

$('.collapse-item .collapse-header').on('click', function(){

  if(!$(this).hasClass('collapse-current')){

    $('.collapse-current + .collapse-body').slideToggle();
    $('.collapse-current').removeClass('collapse-current');

    $(this).toggleClass('collapse-current');
    $(this).find('+ .collapse-body').slideToggle();

  }else{

    $(this).toggleClass('collapse-current');
    $(this).find('+ .collapse-body').slideToggle();

  }

});
