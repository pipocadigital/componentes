$(function() {

  var $tabsContainer = $( '.tabs-external--js' ),
      $tabItem       = $tabsContainer.find( '.tab-link--js' ),
      $tabContent    = $tabsContainer.find( '.tab-content--js' ),
      loadURL        = '/components/tabs-external/dados.html',
      href           = document.location.href.split('#')[1];

  if( $tabsContainer.length ){
  	if( href ){
  		$tabsContainer.find('.active').removeClass('active');
      $('.tab-link--js[data-href="#'+ href +'"]').addClass('active');
      loadTab( '#' + href );
  	}else{
      $('.tab-link--js[data-href="'+ $tabItem.data().href +'"]').addClass('active');
      loadTab( $tabItem.data().href );
    }
    $tabItem.on( 'click', function( e ) {
      e.preventDefault();
      $tabsContainer.find('.active').removeClass('active');
      $( this ).addClass( 'active' );
      loadTab( $( this ).data().href );
    });

    function loadTab( href ){
      var url = loadURL + ' ' + href;
      $tabContent.load( url );
      document.location.href = href;
    }
  }

});
