'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var del = require('del');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('gulp-main-bower-files');
var minifyCss = require('gulp-minify-css');
var flatten = require('gulp-flatten');
var gulpsync = require('gulp-sync')(gulp);
var browserSync = require('browser-sync');

gulp.paths = {
	basePath: 'dist/',
	scripts: 'src/**/*.js',
	scriptsDest: 'dist/assets',
	styles: 'src/**/*.sass',
	stylesDest: 'dist/assets',
	images: 'src/**/*.{jpg,png,gif,svg}',
	imagesDest: 'dist/assets',
	pages: 'src/**/*.html',
	pagesDest: 'dist/',
	fontsDest: 'dist/assets/fonts'
};

gulp.task('browser-sync', function () {
	browserSync({
		server: {
			baseDir: gulp.paths.basePath
		}
	});
});

gulp.task('styles', function () {
	gulp.src([gulp.paths.styles])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(autoprefixer(['last 2 versions', 'ie 8', 'ie 9', '> 1%']))
		.pipe(gulp.dest(gulp.paths.stylesDest))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function () {
	return gulp.src([gulp.paths.scripts, '!src/libs/**/*'])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(concat('main.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest(gulp.paths.scriptsDest))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('pages', function () {
	return gulp.src([gulp.paths.pages])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
			}
		}))
		.pipe(gulp.dest(gulp.paths.pagesDest))
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('clean', function () {
	return del([
		gulp.paths.pagesDest
	]);
});

gulp.task('libs', function () {
	var jsFilter = gulpFilter('**/*.js', {restore: true});
	var cssFilter = gulpFilter('**/*.css', {restore: true});
	var fontFilter = gulpFilter(['**/*.eot', '**/*.woff', '**/*.woff2', '**/*.svg', '**/*.ttf'], {restore: true});

	return gulp.src('./bower.json')
		.pipe(mainBowerFiles())
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
			}
		}))

		// JS
		.pipe(jsFilter)
		.pipe(concat('libs.js'))
		.pipe(rename({suffix: ".min"}))
		.pipe(uglify())
		.pipe(gulp.dest(gulp.paths.scriptsDest))
		.pipe(jsFilter.restore)

		//CSS
		.pipe(cssFilter)
		.pipe(minifyCss({compatibility: 'ie8'}))
		.pipe(concat('libs.min.css'))
		.pipe(gulp.dest(gulp.paths.stylesDest))
		.pipe(cssFilter.restore)

		// Fonts
		.pipe(fontFilter)
		.pipe(flatten())
		.pipe(gulp.dest(gulp.paths.fontsDest));
});

gulp.task('images', function () {
	gulp.src([gulp.paths.images])
		.pipe(cache(imagemin({optimizationLevel: 3, progressive: true, interlaced: true})))
		.pipe(gulp.dest(gulp.paths.imagesDest));
});

gulp.task('watch', function () {
	gulp.watch(gulp.paths.styles, ['styles']);
	gulp.watch(gulp.paths.scripts, ['scripts']);
	gulp.watch(gulp.paths.pages, ['pages']);
	gulp.watch(gulp.paths.images, ['images']);
	browserSync.reload();
});

// Build
gulp.task('build', gulpsync.sync([
	'clean',
	'styles',
	'scripts',
	'pages',
	'images',
	'libs'
	])
);

// Default Task
gulp.task('default', gulpsync.sync([
		'build',
		'watch',
		'browser-sync'
	])
);
