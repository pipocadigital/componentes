# Lists

### Angule

```html
<ul class="list list-icon icon-angle">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li>Item list 03</li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-angle">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li>Item list 03</li>
    </ul>
</div>

### Angule Double

```html
<ul class="list list-icon icon-angle-double">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li>Item list 03</li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-angle-double">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li>Item list 03</li>
    </ul>
</div>

### Caret

```html
<ul class="list list-icon icon-caret">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li>Item list 03</li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-caret">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li>Item list 03</li>
    </ul>
</div>

### Arrow 👉

```html
<ul class="list list-icon icon-arrow">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li>Item list 03</li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-arrow">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li>Item list 03</li>
    </ul>
</div>

### Circle

```html
<ul class="list list-icon icon-circle">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li>Item list 03</li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-circle">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li>Item list 03</li>
    </ul>
</div>

### Sub-lists

```html
<ul class="list list-icon icon-circle">
    <li>Item list 01</li>
    <li>Item list 02</li>
    <li class="sub-list">
        <ul class="list list-icon icon-circle">
            <li>Sub-item list 03</li>
            <li>Sub-item list 04</li>
        </ul>
    </li>
</ul>
```

<div class="componente-pipoca">
    <ul class="list list-icon icon-circle">
        <li>Item list 01</li>
        <li>Item list 02</li>
        <li class="sub-list">
            <ul class="list list-icon icon-circle">
                <li>Sub-item list 03</li>
                <li>Sub-item list 04</li>
            </ul>
        </li>
    </ul>
</div>