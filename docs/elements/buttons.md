# Buttons

### Simple and Outline

```html
<button class="button">Simple</button>
<button class="button button-outline">Simple Outline</button>
```

<div class="componente-pipoca">
    <button class="button">Simple</button>
    <button class="button button-outline">Simple Outline</button>
</div>

### Rounded

```html
<button class="button button-round">Simple rounded</button>
<button class="button button-round button-outline">Outline Rounded</button>
```

<div class="componente-pipoca">
    <button class="button button-round">Simple rounded</button>
    <button class="button button-round button-outline">Outline Rounded</button>
</div>

```html
<button class="button button-round-full">Simple more rounded</button>
<button class="button button-round-full button-outline">More rounded Outline</button>
```

<div class="componente-pipoca">
    <button class="button button-round-full">Simple more rounded</button>
    <button class="button button-round-full button-outline">More rounded Outline</button>
</div>

### Status

Success, Info, Warning e Danger

```html
<button class="button button-success">Success</button>
<button class="button button-info button-round">Info Rounded</button>
<button class="button button-warning button-round-full">Warning more Rounded</button>
<button class="button button-danger button-round-full">Danger Rounded</button>
<button class="button button-danger button-round-full button-outline">Danger Rounded Outline</button>
```

<div class="componente-pipoca">
    <button class="button button-success">Success</button>
    <button class="button button-info button-round">Info Rounded</button>
    <button class="button button-warning button-round-full">Warning more Rounded</button>
    <button class="button button-danger button-round-full">Danger Rounded</button>
    <button class="button button-danger button-round-full button-outline">Danger Rounded Outline</button>
</div>

### Disabled

```html
<button class="button disabled" disabled>Disabled</button>
<button class="button button-round disabled" disabled>Disabled Rounded</button>
<button class="button button-round-full disabled" disabled>Disabled more Rounded</button>
```

<div class="componente-pipoca">
    <button class="button disabled" disabled>Disabled</button>
    <button class="button button-round disabled" disabled>Disabled Rounded</button>
    <button class="button button-round-full disabled" disabled>Disabled more Rounded</button>
</div>

### Text + Icons ✨

```html
<button class="button button-primary button-round icon-left">
    <i class="fa">👍</i> Simple Icon
</button>
<button class="button button-success button-round-full icon-left">
    <i class="fa">🍺</i> Simple Rounded Success Icon
</button>
<button class="button button-success button-round-full icon-right">
    Simple Rounded Success Icon Right <i class="fa">👍</i>
</button>
<button class="button button-info button-outline icon-left">
    <i class="fa">🍺</i> Simple Outline Info Icon
</button>
```

<div class="componente-pipoca">
    <button class="button button-primary button-round icon-left">
        <i class="fa">👍</i> Simple Icon
    </button>
    <button class="button button-success button-round-full icon-left">
        <i class="fa">🍺</i> Simple Rounded Success Icon
    </button>
    <button class="button button-success button-round-full icon-right">
        Simple Rounded Success Icon Right <i class="fa">👍</i>
    </button>
    <button class="button button-info button-outline icon-left">
        <i class="fa">🍺</i> Simple Outline Info Icon
    </button>
</div>

### Only Icons ❗️

```html
<button class="button button-icon">
    <i class="fa">👍</i>
</button>
<button class="button button-icon button-round">
    <i class="fa">👍</i>
</button>
<button class="button button-icon button-round-full">
    <i class="fa">👍</i>
</button>
```

<div class="componente-pipoca">
    <button class="button button-icon">
        <i class="fa">👍</i>
    </button>
    <button class="button button-primary button-outline button-icon button-round">
        <i class="fa">👍</i>
    </button>
    <button class="button button-icon button-round-full">
        <i class="fa">👍</i>
    </button>
</div>

### Sizes

```html
<button class="button button-small">Button</button>
<button class="button button-success">Button</button>
<button class="button button-large">Button</button>
```

<div class="componente-pipoca">
    <button class="button button-small">Button</button>
    <button class="button">Button</button>
    <button class="button button-large">Button</button>
</div>