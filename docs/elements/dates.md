# Dates

### Simple, Outline and Fill

```html
<time class="date" datetime="2016-06-17">17/06/2016</time>
<time class="date date-outline" datetime="2016-06-17">17/06/2016</time>
<time class="date date-fill" datetime="2016-06-17">17/06/2016</time>
```

<div class="componente-pipoca">
    <time class="date" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-outline" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-fill" datetime="2016-06-17">17/06/2016</time>
</div>

### Icons

```html
<time class="date date-icon" datetime="2016-06-17">17/06/2016</time>
<time class="date date-outline date-icon" datetime="2016-06-17">17/06/2016</time>
<time class="date date-fill date-icon" datetime="2016-06-17">17/06/2016</time>
```

<div class="componente-pipoca">
    <time class="date date-icon" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-icon date-outline" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-icon date-fill" datetime="2016-06-17">17/06/2016</time>
</div>

### Rounded

```html
<time class="date date-outline date-round" datetime="2016-06-17">17/06/2016</time>
<time class="date date-fill date-round" datetime="2016-06-17">17/06/2016</time>
```

<div class="componente-pipoca">
    <time class="date date-outline date-round" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-fill date-round" datetime="2016-06-17">17/06/2016</time>
</div>

### Rounded Full and Icons

```html
<time class="date date-outline date-round-full date-icon" datetime="2016-06-17">17/06/2016</time>
<time class="date date-fill date-round-full date-icon" datetime="2016-06-17">17/06/2016</time>
```

<div class="componente-pipoca">
    <time class="date date-outline date-round-full date-icon" datetime="2016-06-17">17/06/2016</time>
    <time class="date date-fill date-round-full date-icon" datetime="2016-06-17">17/06/2016</time>
</div>

### Square and Half

```html
<time class="date date-square" datetime="2016-06-17"><span>17</span>Jun</time>
<time class="date date-half" datetime="2016-06-17"><span>17</span>Jun</time>
```

<div class="componente-pipoca">
    <time class="date date-square" datetime="2016-06-17"><span>17</span>Jun</time>
    <time class="date date-half" datetime="2016-06-17"><span>17</span>Jun</time>
</div>