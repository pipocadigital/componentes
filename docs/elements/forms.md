# Forms

### Input Simple, Rounded and Rounded Full

```html
<label class="input">
    <input type="text" placeholder="Text">
</label>
<label class="input input-round">
    <input type="text" placeholder="Text">
</label>
<label class="input input-round-full">
    <input type="text" placeholder="Text">
</label>
```

<div class="componente-pipoca">
    <label class="input">
        <input type="text" placeholder="Text">
    </label>
    <label class="input input-round">
        <input type="text" placeholder="Text">
    </label>
    <label class="input input-round-full">
        <input type="text" placeholder="Text">
    </label>
</div>

### Input Disabled

```html
<label class="input disabled">
    <input type="text" placeholder="Text" disabled>
</label>
<label class="input input-round disabled">
    <input type="text" placeholder="Text" disabled>
</label>
<label class="input input-round-full disabled">
    <input type="text" placeholder="Text" disabled>
</label>
```

<div class="componente-pipoca">
    <label class="input disabled">
        <input type="text" placeholder="Text" disabled>
    </label>
    <label class="input input-round disabled">
        <input type="text" placeholder="Text" disabled>
    </label>
    <label class="input input-round-full disabled">
        <input type="text" placeholder="Text" disabled>
    </label>
</div>

### Input with Icon

```html
<label class="input input-icon">
    <i class="fa fa-location-arrow"></i>
    <input type="text" placeholder="Address">
</label>
<label class="input input-round input-icon">
    <i class="fa fa-location-arrow"></i>
    <input type="text" placeholder="Address">
</label>
<label class="input input-round-full input-icon icon-right">
    <i class="fa fa-search"></i>
    <input type="text" placeholder="Search">
</label>
```

<div class="componente-pipoca">
    <label class="input input-icon">
        <i class="fa fa-location-arrow"></i>
        <input type="text" placeholder="Address">
    </label>
    <label class="input input-round input-icon">
        <i class="fa fa-location-arrow"></i>
        <input type="text" placeholder="Address">
    </label>
    <label class="input input-round-full input-icon icon-right">
        <i class="fa fa-search"></i>
        <input type="text" placeholder="Search">
    </label>
</div>

### Textarea

```html
<div class="input">
    <textarea></textarea>
</div>
<div class="input input-round">
    <textarea></textarea>
</div>
```

<div class="componente-pipoca">
    <div class="input">
        <textarea></textarea>
    </div>
    <div class="input input-round">
        <textarea></textarea>
    </div>
</div>

### Select

```html
<div class="select">
    <select>
        <option value="option">Select</option>
        <option value="option">option</option>
    </select>
</div>
<div class="select select-round">
    <select>
        <option value="option">Select</option>
        <option value="option">option</option>
    </select>
</div>
<div class="select select-round-full">
    <select>
        <option value="option">Select</option>
        <option value="option">option</option>
    </select>
</div>
```

<div class="componente-pipoca">
    <div class="select">
        <select>
            <option value="option">Select</option>
            <option value="option">option</option>
        </select>
    </div>
    <div class="select select-round">
        <select>
            <option value="option">Select</option>
            <option value="option">option</option>
        </select>
    </div>
    <div class="select select-round-full">
        <select>
            <option value="option">Select</option>
            <option value="option">option</option>
        </select>
    </div>
</div>

### Select Disabled

```html
<div class="select disabled">
    <select disabled>
        <option value="option">Select</option>
    </select>
</div>
<div class="select select-round disabled">
    <select disabled>
        <option value="option">Select</option>
    </select>
</div>
<div class="select select-round-full disabled">
    <select disabled>
        <option value="option">Select</option>
    </select>
</div>
```

<div class="componente-pipoca">
    <div class="select disabled">
        <select disabled>
            <option value="option">Select</option>
        </select>
    </div>
    <div class="select select-round disabled">
        <select disabled>
            <option value="option">Select</option>
        </select>
    </div>
    <div class="select select-round-full disabled">
        <select disabled>
            <option value="option">Select</option>
        </select>
    </div>
</div>


### Checkbox

```html
<label class="checkbox">
    <input type="checkbox" name="checkbox" checked>
    <input type="checkbox" name="checkbox">
</label>
<label class="checkbox checkbox-round">
    <input type="checkbox" name="checkbox" checked>
    <input type="checkbox" name="checkbox">
</label>
<label class="checkbox checkbox-round-full">
    <input type="checkbox" name="checkbox" checked>
    <input type="checkbox" name="checkbox">
</label>
```

<div class="componente-pipoca">
    <label class="checkbox">
        <input type="checkbox" name="checkbox" checked>
        <input type="checkbox" name="checkbox">
    </label>
    <label class="checkbox checkbox-round">
        <input type="checkbox" name="checkbox" checked>
        <input type="checkbox" name="checkbox">
    </label>
    <label class="checkbox checkbox-round-full">
        <input type="checkbox" name="checkbox" checked>
        <input type="checkbox" name="checkbox">
    </label>
</div>

### Checkbox with status

```html
<label class="checkbox checkbox-success">
    <input type="checkbox" name="checkbox" checked>
</label>
<label class="checkbox checkbox-danger checkbox-round">
    <input type="checkbox" name="checkbox" checked>
</label>
<label class="checkbox checkbox-warning checkbox-round-full">
    <input type="checkbox" name="checkbox" checked>
</label>
<label class="checkbox checkbox-info checkbox-round-full">
    <input type="checkbox" name="checkbox" checked>
</label>
```

<div class="componente-pipoca">
    <label class="checkbox checkbox-success">
        <input type="checkbox" name="checkbox" checked>
    </label>
    <label class="checkbox checkbox-danger checkbox-round">
        <input type="checkbox" name="checkbox" checked>
    </label>
    <label class="checkbox checkbox-warning checkbox-round-full">
        <input type="checkbox" name="checkbox" checked>
    </label>
    <label class="checkbox checkbox-info checkbox-round-full">
        <input type="checkbox" name="checkbox" checked>
    </label>
</div>


### Checkbox Disabled

```html
<label class="checkbox">
    <input type="checkbox" name="checkbox" disabled>
</label>
<label class="checkbox checkbox-round">
    <input type="checkbox" name="checkbox" disabled>
</label>
<label class="checkbox checkbox-round-full">
    <input type="checkbox" name="checkbox" disabled checked>
</label>
```

<div class="componente-pipoca">
    <label class="checkbox">
        <input type="checkbox" name="checkbox" disabled>
    </label>
    <label class="checkbox checkbox-round">
        <input type="checkbox" name="checkbox" disabled>
    </label>
    <label class="checkbox checkbox-round-full">
        <input type="checkbox" name="checkbox" disabled checked>
    </label>
</div>

### Radio Button

```html
<label class="radio" for="radio1-1">
    <input id="radio1-1" type="radio" name="radio1" checked="checked">
    <span class="inner"></span>
</label>
<label class="radio" for="radio1-2">
    <input id="radio1-2" type="radio" name="radio1">
    <span class="inner"></span>
</label>
```

<div class="componente-pipoca">
    <label class="radio" for="radio1-1">
        <input id="radio1-1" type="radio" name="radio1" checked="checked">
        <span class="inner"></span>
    </label>
    <label class="radio" for="radio1-2">
        <input id="radio1-2" type="radio" name="radio1">
        <span class="inner"></span>
    </label>
</div>

### Radio Button with status

```html
<label class="radio radio-success" for="radio2-1">
    <input id="radio2-1" type="radio" name="radio2" checked="checked">
    <span class="inner"></span>
</label>
<label class="radio radio-danger" for="radio2-2">
    <input id="radio2-2" type="radio" name="radio2">
    <span class="inner"></span>
</label>
<label class="radio radio-warning" for="radio2-3">
    <input id="radio2-3" type="radio" name="radio2">
    <span class="inner"></span>
</label>
<label class="radio radio-info" for="radio2-4">
    <input id="radio2-4" type="radio" name="radio2">
    <span class="inner"></span>
</label>
```

<div class="componente-pipoca">
    <label class="radio radio-success" for="radio2-1">
        <input id="radio2-1" type="radio" name="radio2" checked="checked">
        <span class="inner"></span>
    </label>
    <label class="radio radio-danger" for="radio2-2">
        <input id="radio2-2" type="radio" name="radio2">
        <span class="inner"></span>
    </label>
    <label class="radio radio-warning" for="radio2-3">
        <input id="radio2-3" type="radio" name="radio2">
        <span class="inner"></span>
    </label>
    <label class="radio radio-info" for="radio2-4">
        <input id="radio2-4" type="radio" name="radio2">
        <span class="inner"></span>
    </label>
</div>


### Radio Button Disabled

```html
<label class="radio" for="radio3-1">
    <input id="radio3-1" type="radio" name="radio3" checked="checked" disabled>
    <span class="inner"></span>
</label>
<label class="radio" for="radio3-2">
    <input id="radio3-2" type="radio" name="radio3" disabled>
    <span class="inner"></span>
</label>
```

<div class="componente-pipoca">
    <label class="radio" for="radio3-1">
        <input id="radio3-1" type="radio" name="radio3" checked="checked" disabled>
        <span class="inner"></span>
    </label>
    <label class="radio" for="radio3-2">
        <input id="radio3-2" type="radio" name="radio3" disabled>
        <span class="inner"></span>
    </label>
</div>
