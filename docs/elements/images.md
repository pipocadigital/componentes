# Images

### Simple, Rounded and Circle

```html
<img class="image" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
<img class="image image-round" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
<img class="image image-circle" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
```

<div class="componente-pipoca">
    <img class="image" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
    <img class="image image-round" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
    <img class="image image-circle" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
</div>

### Simple, Rounded and Circle with border

```html
<img class="image image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
<img class="image image-round image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
<img class="image image-circle image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
```

<div class="componente-pipoca">
    <img class="image image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
    <img class="image image-round image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
    <img class="image image-circle image-border" src="https://placeholdit.imgix.net/~text?txtsize=30&txt=image&w=200&h=200" alt="">
</div>